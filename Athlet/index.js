const mysql = require('mysql')

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'Athele',
    password: 'Athele',
    database: 'national team athelete'
});

connection.connect();


const express = require('express')
const app = express()
const port = 4000


/* API for Registering a new sport */
app.post("/register_sport", (req, res) => {

    let sport_name = req.query.sport_name

    let query = `INSERT INTO sport
                (SportName)  
                VALUES ('${sport_name}')`
    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            res.json({
                "status": "400",
                "message": "Error inserting data into db"
            });
        } else {
            res.json({
                "status": "200",
                "message": "Adding athele succesful"
            })
        }
    });
});

app.post("/update_athele", (req, res) => {

    let athele_id = req.query.athele_id
    let athele_name = req.query.athele_name


    let query = `UPDATE athele SET 
                    AtheleName = '${athele_name}'
                    WHERE AtheleID = ${athele_id}`

    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error updating record "
            });
        } else {
            res.json({
                "status": "200",
                "message": "updating athele succesful"
            })
        }
    });


});

app.post("/delete_athele", (req, res) => {

    let athele_id = req.query.athele_id

    let query = `DELETE FROM athele  WHERE AtheleID = ${athele_id}`

    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error deleteting record "
            });
        } else {
            res.json({
                "status": "200",
                "message": "deleteting record succesful"
            })
        }
    });


});

/* CRUD Operation for athele Table*/
app.get("/list_athele", (req, res) => {
    let query = "SELECT * From athele";
    connection.query(query, (err, rows) => {
        if (err) {
            res.json({
                "status": "400",
                "message": "Error querying from National Team Athelete db"
            });
        } else {
            res.json(rows)
        }
    });
});


app.post("/add_athele", (req, res) => {
    let athele_name = req.query.athele_name
    let athele_country = req.query.athele_country

    let query = `INSERT INTO athele 
                (AtheleName, AtheleCountry)  
                VALUES ('${athele_name}','${athele_country} ')`
    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            res.json({
                "status": "400",
                "message": "Error inserting data into db"
            });
        } else {
            res.json({
                "status": "200",
                "message": "Adding athele succesful"
            })
        }
    });


});

app.post("/update_athele", (req, res) => {

    let athele_id = req.query.athele_id
    let athele_name = req.query.athele_name
    let athele_country = req.query.athele_country

    let query = `UPDATE athele SET 
                    AtheleName = '${athele_name}',
                    AtheleCountry = '${athele_country}'
                    WHERE AtheleID = ${athele_id}`

    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error updating record "
            });
        } else {
            res.json({
                "status": "200",
                "message": "updating athele succesful"
            })
        }
    });


});


app.post("/delete_athele", (req, res) => {

    let athele_id = req.query.athele_id

    let query = `DELETE FROM athele  WHERE AtheleID = ${athele_id}`

    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status": "400",
                "message": "Error deleteting record "
            });
        } else {
            res.json({
                "status": "200",
                "message": "deleteting record succesful"
            })
        }
    });


});


app.listen(port, () => {
    console.log(`Now starting National Team Athelete Backend ${port}`)

});



/*
Query = "SELECT * from sport";
connection.query(Query, (err, rows) => {
    if (err) {
        console.log(err);
    } else {
        console.log(rows);
    }

});

connection.end();*/